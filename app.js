const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors')
var app = express();
var path = require('path');
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.set('socketio', io);

//morgan middleware
app.use(morgan('tiny'))
app.use(express.static(__dirname, { dotfiles: 'allow' } ));

//cors 
app.use(cors())

// //body-parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// view engine setup
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');

// set path for static assets
var assetsPath = path.join(__dirname, '/public');
app.use(express.static(assetsPath));

//exposed ports
const port = process.env.PORT || 80;


io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('join', function (data) {
        socket.join(data); // We are using room of socket io
        // io.in(data).emit('new_msg', { msg: 'hello' });
        console.log('a user connected to - '+data)
    });
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});


//routes
const index = require('./routes/index');
app.use('/', index);

const api = require('./routes/api');
app.use('/api',api)

app.get('/sitemap.xml', function(req, res){
    res.send('./public/sitemap.xml')
});

app.get('*', function(req, res){
    res.status(404).render("404")
});

app.listen(port, () => {
    console.log('Api listening on :' + port)
})

http.listen(9999, function () {
    console.log('Socket listening on :' + 9999);
});