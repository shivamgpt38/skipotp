var express = require('express');
var router = express.Router();


// GET HomePage

let numbers = {
    ca: [12262108433, 12262422867, 12262770690, 12262770693, 12262770694, 12262770696, 12262770713, 12262770721, 12262770739],
    us: [12013514503, 12013537852, 12013553026, 12013657145, 12013657182, 12013657906]
}
let flag = {
    ca: "https://freeconvertonline.sgp1.digitaloceanspaces.com/skipotp.com/27114.jpg",
    us: "https://freeconvertonline.sgp1.digitaloceanspaces.com/skipotp.com/18165.jpg"
}
let country = {
    ca: "Canada",
    us: "USA"
}

router.get('/number/:phone', (req, res, next) => {
    // var io = req.app.get('socketio');
    // io.in(req.params.phone).emit('new_msg', { msg: req.params.phone });
    console.log(req.query)
    if (!req.query.to || !req.query.msisdn) {
        console.log('This is not a valid inbound SMS message!');
    } else {
        if (req.query.concat) {
            console.log('Fail: the message is too long.');
        } else {
            console.log('Success');
            let incomingData = {
                messageId: req.query.messageId,
                from: req.query.msisdn,
                text: req.query.text,
                timestamp: req.query['message-timestamp']
            };
            // let incomingData = {
            //     "messageId": "170000025A590CDB",
            //     "from": "918076185132",
            //     "text": "Testing for live server",
            //     "timestamp": "2019-11-16 05:49:02"
            // }
            var io = req.app.get('socketio');
            io.in(req.params.phone).emit('new_msg', { msg: incomingData });
        }
    }
    res.status(200).end();
})

router.get('/country/:code', (req, res, next) => {
    res.json({ 'numbers': numbers[req.params.code], "flag": flag[req.params.code], "country": country[req.params.code] })
})


module.exports = router;