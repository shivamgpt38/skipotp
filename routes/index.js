var express = require('express');
var router = express.Router();

// GET HomePage
router.get('/', function (req, res, next) {
    res.render('index');
});

router.get('/number/:phone',(req,res,next)=>{
    res.render('number',{phone:req.params.phone});
})

router.get('/country/:code',(req,res,next)=>{
    res.render('country',{cc:req.params.code})
})



module.exports = router;