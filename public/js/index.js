$(document).ready(function () {
    var accordions = bulmaAccordion.attach();
    var app = document.getElementById('advantage');

    var typewriter = new Typewriter(app, {
        loop: true
    });

    typewriter.typeString('test your server')
        .pauseFor(2500)
        .deleteAll()
        .typeString('skip OTP')
        .pauseFor(2500)
        .deleteAll()
        .start();

    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function (e) {
        console.log(e)
        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
    });

});
